package nutshell.nullSafety

import nutshell.nullSafety.model.Persoon
import nutshell.nullSafety.service.TitulatuurService
import org.junit.Test
import kotlin.test.assertSame

class TitalatuurTest {

    val titalatuurService = TitulatuurService()

    @Test
    fun getFullNaamTest(){
        val persoon = Persoon("Paul", "J.P.", "McCartney")
        val titulatuur = titalatuurService.getTitulatuur(persoon)
        assertSame("Beste J.P. McCartney", titulatuur)
    }

    @Test
    fun getZonderVoornaamTest(){
        val persoon = Persoon(null, "J.P.", "McCartney")
        val titulatuur = titalatuurService.getTitulatuur(persoon)
        assertSame("Beste J.P. McCartney", titulatuur)
    }

    @Test
    fun getZonderVoorlettersTest(){
        val persoon = Persoon("Paul", null, "McCartney")
        val titulatuur = titalatuurService.getTitulatuur(persoon)
        assertSame("Beste Paul McCartney", titulatuur)
    }

    @Test
    fun getMinimumPersooNTest(){
        val persoon = Persoon(null, null, "McCartney")
        val titulatuur = titalatuurService.getTitulatuur(persoon)
        assertSame("Beste meneer McCartney", titulatuur)
    }
}

