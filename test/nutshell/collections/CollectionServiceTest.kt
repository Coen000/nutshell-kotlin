package nutshell.collections

import nutshell.collections.service.CollectionService
import org.junit.Test
import java.lang.UnsupportedOperationException
import kotlin.test.assertSame

class CollectionServiceTest {

    val collectionService = CollectionService()

    @Test
    fun intToStringTest(){
        val values = listOf(1, 33, 999)
        val result = collectionService.intToString(values)

        assertSame("1", result[0])
        assertSame("33", result[1])
        assertSame("999", result[2])
    }

    @Test
    fun getMaximumValueTest(){
        val list1 = listOf(1,2,3)
        val list2 = listOf(99,16,2)
        val list3 = listOf(-99,88,-2)

        assertSame(collectionService.getMaximumValue(list1), 3)
        assertSame(collectionService.getMaximumValue(list2), 99)
        assertSame(collectionService.getMaximumValue(list3), 88)
    }

    @Test
    fun getMaximumValuesTest(){
        val list1 = listOf(1,2,3)
        val list2 = listOf(99,16,2)
        val list3 = listOf(-99,88,-2)

        // Als je volgorde alwijkt, mag je de test aanpassen zodat deze wel slaagt
        assertSame(collectionService.getMaximumValues(list1, 2), listOf(3,2))
        assertSame(collectionService.getMaximumValues(list1, 3), listOf(3, 2, 1))
        assertSame(collectionService.getMaximumValues(list2, 1), listOf(99))
        assertSame(collectionService.getMaximumValues(list3, 1), listOf(88, -2))
    }

    @Test(expected = UnsupportedOperationException::class)
    fun multiplyListValuesCantBeDifferentSizeTest(){
        val list1 = listOf(3, 4)
        val list2 = listOf(3, 4, 5)
        val result = collectionService.multiplyListValues(list1, list2)
    }

    @Test
    fun multiplyListValuesTest(){
        val list1 = listOf(3, 4)
        val list2 = listOf(2, 4)
        val result = collectionService.multiplyListValues(list1, list2)
        assertSame(6, result[0])
        assertSame(16, result[1])
    }
}
