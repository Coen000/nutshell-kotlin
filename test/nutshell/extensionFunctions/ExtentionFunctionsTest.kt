package nutshell.extensionFunctions

import org.junit.Test
import kotlin.test.assertSame

class ExtentionFunctionsTest {

    @Test
    fun toKwadraadMinusOne(){
        //een extensie op het Int type: Vermenigvuldig hem met zichzelf, maar haal er eerst 1 af
        val result = 4.toKwadraadMinusOne() //4*3
        assertSame(12, result)
        assertSame(72, 9.toKwadraadMinusOne() )
        assertSame(90, 10.toKwadraadMinusOne())
        assertSame(6, (-3).toKwadraadMinusOne())
    }

    @Test
    fun countNullValues(){
        val list = listOf(1, null, 2, null, 3)
        val result = list.countNullValues()
        assertSame(result, 2)
        assertSame(listOf(null, null, 2, null, null).countNullValues(), 2)
    }

    @Test
    fun addToFront(){
        val list = mutableListOf(1,2,3)
        val result = list.addToFrontOfList(4)
        assertSame(4, result.size)
        assertSame(result[0], 4)
        assertSame(result[3], 3)
    }
}

private fun Int.toKwadraadMinusOne(): Int {
    TODO("not implemented")
}

private fun List<Int?>.countNullValues(): Int {
    TODO("not implemented")
}

private fun MutableList<Int>.addToFrontOfList(toAdd: Int): MutableList<Int> {
    TODO("not implemented")
}
