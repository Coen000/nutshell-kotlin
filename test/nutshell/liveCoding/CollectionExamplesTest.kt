package nutshell.liveCoding

import org.junit.Assert.assertTrue
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class CollectionExamplesTest {

    val collectionExamples = CollectionExamples()

    @Test
    fun printCollectionTest() {
        val list = listOf<String>("1", "2", "3")
        collectionExamples.printCollection(list)
    }

    @Test
    fun addOneToEachTest() {
        val list = listOf(1, 2, 3)
        val result = collectionExamples.addOneToEach(list)

        // Check of er 1 is opgeteld bij elk element
        assertSame(2, result[0])
        assertSame(3, result[1])
        assertSame(4, result[2])
    }

    @Test
    fun combineCollectionsTest() {
        val list1 = listOf(1, 2)
        val list2 = listOf(3, 4)
        val result1 = collectionExamples.combineCollections(list1, list2)

        // Check of het resultaat een collectie met 1,2,3 en 4 is. De volgorde maakt niet uit
        assertEquals(4, result1.size)
        assertTrue(result1.contains(1))
        assertTrue(result1.contains(2))
        assertTrue(result1.contains(3))
        assertTrue(result1.contains(4))

        // Nu met een String
        val list3 = listOf("a", "b")
        val list4 = listOf("c", "d")
        val result2 = collectionExamples.combineCollections(list3, list4)

        // Check of het resultaat een collectie met a,b,c en d is. De volgorde maakt niet uit
        assertEquals(4, result2.size)
        assertTrue(result2.contains("a"))
        assertTrue(result2.contains("b"))
        assertTrue(result2.contains("c"))
        assertTrue(result2.contains("d"))
    }

}