package nutshell.liveCoding

import org.junit.Test
import kotlin.test.assertSame

class ExtensionExamplesTest {

    val extensionExamples = Cijferlijst()

    @Test
    fun afgerondeCijfersTest(){

        val afgerondCijfer = extensionExamples.getAfgerondCijfer(8.8)
        assertSame(9, afgerondCijfer)
    }

    @Test
    fun afgerondeCijfersTestPositief(){

        val afgerondCijfer = extensionExamples.getAfgerondCijfer(8.3)
        assertSame(8, afgerondCijfer)

        val afgerondCijfer2 = extensionExamples.getAfgerondCijfer(8.4)
        assertSame(9, afgerondCijfer2)
    }
}
