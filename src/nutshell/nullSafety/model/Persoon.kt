package nutshell.nullSafety.model

data class Persoon(
    val voornaam: String?,
    val voorletters: String?,
    val achternaam: String,
    val man: Boolean = true
)

