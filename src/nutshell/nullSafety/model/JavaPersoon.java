package nutshell.nullSafety.model;

class JavaPersoon{

    private String voornaam;
    private String voorletters;
    private String achternaam;

    JavaPersoon(String voornaam, String voorletters, String achternaam){
        this.voornaam = voornaam;
        this.voorletters = voornaam;
        this.achternaam = voornaam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public String getVoorletters() {
        return voorletters;
    }

    public String getAchternaam(){
        return achternaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public void setVoorletters(String voorletters) {
        this.voorletters = voorletters;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }
}