package nutshell.liveCoding

class CollectionExamples {

    /**
     * Collection: foreach
     */
    fun printCollection(collection: Collection<String>) {

    }

    /**
     * Collection: map
     * Extention function
     */
    fun addOneToEach(collectie: List<Int>): List<Int> {
        return collectie
    }

    /**
     * Collections zijn standaard immutable
     */
    fun <E> combineCollections(collection1: Collection<E>, collection2: Collection<E>): List<E> {
        return collection1.toList()
    }


}

