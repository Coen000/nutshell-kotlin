package nutshell.liveCoding

import kotlin.math.round


class Cijferlijst {

    fun getAfgerondCijfer(cijfer: Double): Int{
        return round(cijfer).toInt()
    }
}
